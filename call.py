from papa import series
if __name__ == '__main__':
    x=float(input())
    n=int(input())
    data_str = [
             '---------------',
            '|Номер|Значение |',
            '|---------------|'
        ]
    for i in range(1, n+1):
        data_str.append(f'|{i:^5}|{series.item.argument(x,i):^ 9.3}|')
        data_str.append('|---------------|')
    data_str.append(f'|Cумма|{series.summa(x,n):^ 9.3}|')
    data_str.append('---------------')
    print('\n'.join(data_str))

