def argument(x,i):
    def fac(x,i):
        k=1
        for i in range (1,i+1):
            k*=1
        return k
    arg=((-1)**(i-1))*((i-1)/fac(x,i))*x**i
    return arg
